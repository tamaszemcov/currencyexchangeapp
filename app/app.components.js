﻿angular.module("currencyExchangeApp")
    .component("currencyExchange", {
        templateUrl: "app/partials/currencyExchange.html",
        controller: "CurrencyExchangeController"
    });
