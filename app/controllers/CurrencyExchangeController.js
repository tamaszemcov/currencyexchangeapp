﻿angular.module("currencyExchangeApp")
    .controller("CurrencyExchangeController", ["$scope", "$http", function CurrencyExchangeController($scope, $http) {

        // Currency Exchange

        $scope.fromCurrency = "USD";
        $scope.toCurrency = "USD";
        $scope.fromValue = 0;


        $scope.changeCurrency = function () {

            $http({
                method: 'GET',
                url: '/api/rates?from=' + $scope.fromCurrency + '&to=' + $scope.toCurrency + '&value=' + $scope.fromValue
            }).then(function successCallback(response) {
                $scope.toValue = response.data;

            }, function errorCallback(response) {
                console.log("http error");
            });

        }
        
        // Currency Rates

        var graph;

        function renderChart() {
            $http({
                method: 'GET',
                url: '/api/rates?currency=' + $scope.chartCurrency
            }).then(function successCallback(response) {
                var points = [];
                var times = [];

                var j = 0;
                for (var i = response.data.length - 1; i > -1; i--) {
                    points.push({
                        x: j,
                        y: response.data[i].Rate
                    });

                    times.push(response.data[i].Time);
                    j++;
                }

                graph = new Rickshaw.Graph({
                    element: document.getElementById("chart"),
                    width: window.innerWidth/12*8 - 53,
                    height: 500,
                    renderer: 'area',
                    series: [{
                        color: 'steelblue',
                        name: $scope.chartCurrency,
                        data: points
                    }]
                });

                graph.render();

                var hoverDetail = new Rickshaw.Graph.HoverDetail({
                    graph: graph,
                    formatter: function (series, x, y) {
                        var date = '<span class="date">' + times[x] + '</span>';                        
                        var content = series.name + ": " + y + '<br>' + date;
                        return content;
                    }
                });

                var xAxis = new Rickshaw.Graph.Axis.X({
                    graph: graph
                });
                xAxis.render();

                var yAxis = new Rickshaw.Graph.Axis.Y({
                    graph: graph,
                    tickFormat: Rickshaw.Fixtures.Number.formatKMBT
                });

                yAxis.render();




            }, function errorCallback(response) {
                console.log("http error");
            });
        }
       
        // create chart
        $scope.chartCurrency = "USD";
        renderChart();
   

        $scope.changeChart = function () {

            $('#chart_container').html(
              '<div id="chart"></div>'
            );         
            renderChart();
        }

    }]);