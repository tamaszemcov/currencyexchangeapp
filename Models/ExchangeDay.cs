﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CurrencyExchangeApp.Models
{
    public class ExchangeDay
    {
        public DateTime Time { get; set; }
        public List<CurrencyRate> CurrencyRates { get; set; }
    }
}