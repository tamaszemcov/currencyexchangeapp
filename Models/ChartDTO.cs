﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CurrencyExchangeApp.Models
{
    public class ChartDTO
    {
        public DateTime Time { get; set; }
        public double Rate { get; set; }
    }
}