﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml;
using CurrencyExchangeApp.Models;
using System.Globalization;

namespace CurrencyExchangeApp.Controllers
{
    public class RatesController : ApiController
    {

        List<ExchangeDay> exchangeDays;

        // Getting data from xml feed
        public RatesController()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load("http://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml");

            XmlNodeList nodes = doc.FirstChild.NextSibling.FirstChild.NextSibling.NextSibling.ChildNodes;

            exchangeDays = new List<ExchangeDay>();

            foreach (XmlNode node in nodes)
            {
                ExchangeDay day = new ExchangeDay();

                day.Time = DateTime.Parse(node.Attributes["time"].Value);

                XmlNodeList rates = node.ChildNodes;
                List<CurrencyRate> CurrencyRates = new List<CurrencyRate>();
                foreach (XmlNode rate in rates)
                {
                    CurrencyRate currencyRate = new CurrencyRate();
                    currencyRate.Currency = rate.Attributes["currency"].Value;

                    NumberFormatInfo provider = new NumberFormatInfo();
                    provider.NumberDecimalSeparator = ".";                    

                    currencyRate.Rate = Convert.ToDouble(rate.Attributes["rate"].Value, provider);
                    CurrencyRates.Add(currencyRate);
                }

                day.CurrencyRates = CurrencyRates;
                exchangeDays.Add(day);
    
            }

        }

        // GET api/rates
        // Returns the rates for a given currency for the graph
        public List<ChartDTO> Get([FromUri] string currency)
        {
            List<ChartDTO> data = new List<ChartDTO>();
            foreach (ExchangeDay day in exchangeDays)
            {
                ChartDTO chartDTO = new ChartDTO();
                chartDTO.Time = day.Time;                

                foreach (CurrencyRate currencyRate in day.CurrencyRates)
                {
                    if (String.Equals(currencyRate.Currency, currency))
                    {
                        chartDTO.Rate = currencyRate.Rate;
                    }
                }


                data.Add(chartDTO);
            }

            return data;
        }

        // GET api/rates
        // Returns the calculated currency value for exchange
        public double Get([FromUri] string from, [FromUri] string to, [FromUri] double value)
        {
            double fromRate = 1;
            double toRate = 1;
            double fromValue = value;

            foreach (CurrencyRate currencyRate in exchangeDays[0].CurrencyRates)
            {
                if (String.Equals(currencyRate.Currency, from))
                {
                    fromRate = currencyRate.Rate;
                }

                if (String.Equals(currencyRate.Currency, to))
                {
                    toRate = currencyRate.Rate;
                }
            }

            double EURValue = fromValue / fromRate;
            double toValue = EURValue * toRate;

            return toValue;
        }
    }
}
